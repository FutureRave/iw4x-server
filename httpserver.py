from http.server import HTTPServer, BaseHTTPRequestHandler
from datetime import datetime

from os import listdir
from os.path import isfile, join, getsize

import hashlib

import json

net_port = 28960

fs_game_dir = 'mods/arzxsimon'
fs_base_path = ''

class HttpServer(BaseHTTPRequestHandler):

    def do_GET(self):

        if self.path == '/list':
            json_response = []
            path = join(fs_base_path, fs_game_dir)
            files = [f for f in listdir(path) if isfile(join(path, f)) and '_svr_' not in f]
            files = [f for f in files if f == 'mod.ff' or '.iwd' in f]

            for file in files:
                file_path = join(path, file)
                size = getsize(file_path)
                sha256 = hashlib.sha256()

                with open(file_path, 'rb') as f:
                    while True:
                        data = f.read(4096)
                        if not data:
                            break
                        sha256.update(data)

                json_response.append({
                    'name': file,
                    'size': size,
                    'hash': sha256.hexdigest().upper()
                })

            self.send_response(200)
            self.send_header('Content-Type', 'application/json')
            self.end_headers()
            self.wfile.write(bytes(json.dumps(json_response), 'utf-8'))

        elif self.path.startswith('/file'):
            uri = self.path.replace('\\', '/')
            uri = uri[6:]

            if '_svr_' in uri:
                m = 'Forbidden'
                self.send_error(403, m)

            if '.iwd' not in uri and 'mod.ff' not in uri:
                m = 'Forbidden'
                self.send_error(403, m)

            path = join(fs_base_path, fs_game_dir)
            file = join(path, uri)

            try:
                with open(file, 'rb') as f:
                    content = f.read()

                    self.send_response(200)
                    self.send_header('Content-Type', 'application/octet-stream')
                    self.send_header('Content-length', str(len(content)))
                    self.send_header("Connection", "close")
                    self.end_headers()
                    self.wfile.write(content)

            except FileNotFoundError:
                m = 'Not Found ' + self.path
                self.send_error(400, m)
        else:
            m = 'Unsupported URI: ' + self.path
            self.send_error(400, m)


httpd = HTTPServer(('localhost', net_port), HttpServer)
httpd.serve_forever()
